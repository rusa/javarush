package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList()
    {
        //напишите тут ваш код
        ArrayList<String> aList = new ArrayList<String>();
        aList.add("asd asd dddasd ");
        aList.add("kjllkjlkjlkjlk");

        ArrayList<String>[] arrOfList = new ArrayList[3];
        arrOfList[0] = aList;
        arrOfList[1] = aList;
        arrOfList[2] = aList;

        return arrOfList;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}
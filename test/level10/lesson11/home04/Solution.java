package com.javarush.test.level10.lesson11.home04;

/* Большая зарплата
Вывести на экран надпись «Я не хочу изучать Java, я хочу большую зарплату» 40 раз по образцу.
Образец:
Я не хочу изучать Java, я хочу большую зарплату
 не хочу изучать Java, я хочу большую зарплату
не хочу изучать Java, я хочу большую зарплату
е хочу изучать Java, я хочу большую зарплату
 хочу изучать Java, я хочу большую зарплату
хочу изучать Java, я хочу большую зарплату
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        String s = "Я не хочу изучать Java, я хочу большую зарплату";

        //напишите тут ваш код
        //Вариант 1
//        for(int i = 0; i<40; i++){
//            for(int j=i; j<s.length(); j++){
//                System.out.print(s.charAt(j));
//            }
//            System.out.println();
//        }


        //Вариант 2
        StringBuilder stringBuilder = new StringBuilder(s);

        for(int i = 0; i<40; i++){
            System.out.println(stringBuilder.toString());
            stringBuilder.deleteCharAt(0);
        }
    }
}

package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конструкторы
        boolean sex;
        Integer age;
        String name;
        Integer height;
        Integer weight;
        boolean maried;

        Human (){
        }
        Human (Boolean sex ){
            this.sex = sex;
        }
        Human (Boolean sex, Integer age, String name, Integer height){
            this.age = age;
            this.sex = sex;
            this.maried = false;
            this.name = name;
            this.height = height;
        }


        Human (Boolean sex, Integer age, String name ){
            this.age = age;
            this.sex = sex;
            this.maried = false;
            this.name = name;
        }

        Human (Boolean sex, Integer age, String name, Integer height, Integer weight, Boolean maried ){
            this.age = age;
            this.sex = sex;
            this.maried = maried;
            this.name = name;
            this.height = height;
            this.weight = weight;
        }
        Human (Boolean sex, Integer age){
            this.age = age;
            this.sex = sex;

        }
        Human (Boolean sex, Integer age, String name, Integer height, Integer weight){
            this.age = age;
            this.sex = sex;
            this.maried = false;
            this.name = name;
            this.height = height;
            this.weight = weight;
        }
        Human (Boolean sex, Boolean maried ){
            this.sex = sex;
            this.maried = maried;

        }
        Human (Boolean sex, String name, Integer height, Integer weight, boolean maried ){
            this.sex = sex;
            this.maried = maried;
            this.name = name;
            this.height = height;
            this.weight = weight;
        }
        Human (Boolean sex,  Integer height, Integer weight){
            this.sex = sex;
            this.height = height;
            this.weight = weight;
        }

    }
}

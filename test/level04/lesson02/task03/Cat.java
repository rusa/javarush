package com.javarush.test.level04.lesson02.task03;

/* Реализовать метод setCatsCount
Реализовать метод setCatsCount так, чтобы с его помощью можно было устанавливать значение переменной catsCount равное переданному параметру.
*/

public class Cat {
    private static int catsCount = 0;

    public static void setCatsCount(int catsCoun) {
        //напишите тут ваш код
        catsCount = catsCoun;
    }
    public static void main(String[] args){
        setCatsCount(6);
        System.out.print(catsCount);

    }

}

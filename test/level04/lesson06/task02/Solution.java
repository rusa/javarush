package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        int i =0;
        Integer max = null;
        while (i < 4){
            String a = bufferedReader.readLine();
            Integer newMax = Integer.parseInt(a);

            if(i == 0){
                max = newMax;
            }
            if (newMax > max){
              max = newMax;
            }
            i++;
        }
        System.out.println(max);


    }
}

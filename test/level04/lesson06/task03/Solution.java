package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        int i =0;
        Integer a = null, b = null, c = null;


        while (i < 3){
            String number = bufferedReader.readLine();
            Integer newMax = Integer.parseInt(number);

            if(i == 0){
                a = newMax;
            }
            if(i == 1){
                b = newMax;
            }
            if(i == 2){
                c = newMax;
            }
            i++;
        }
        if (a >= b && b >= c ){
            System.out.println(a);System.out.println(b);System.out.println(c);
        } else
        if (a >= c && c >= b ){
            System.out.println(a);System.out.println(c);System.out.println(b);
        } else
        if (c >= b && b >= a ){
            System.out.println(c);System.out.println(b);System.out.println(a);
        } else
        if (c >= a && a >= b ){
            System.out.println(c);System.out.println(a);System.out.println(b);
        } else
        if (b >= a && a >= c ){
            System.out.println(b);System.out.println(a);System.out.println(c);
        }
         else{
            System.out.println(b);System.out.println(c);System.out.println(a);
        }

    }
}

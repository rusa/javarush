package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        //напишите тут ваш код
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String a = bufferedReader.readLine();

//        InputStream inputStream = System.in;
//        Reader inputStreamReader = new InputStreamReader(inputStream);
//        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String b = bufferedReader.readLine();
        String c = bufferedReader.readLine();

        Integer ai = Integer.parseInt(a);
        Integer bi = Integer.parseInt(b);
        Integer ci = Integer.parseInt(c);

        if( (bi<ai & ai<ci) || (ci<ai & ai<bi) ){
            System.out.println(ai);
        }
        if( (ai<bi & bi<ci) || (ci<bi & bi<ai) ) {
            System.out.println(bi);
        }
        if( (ai<ci & ci<bi) || (bi<ci & ci<ai)){
            System.out.println(ci);
        }

    }
}

package com.javarush.test.level04.lesson16.home03;

import java.io.*;



/* Посчитать сумму чисел
Вводить с клавиатуры числа и считать их сумму. Если пользователь ввел -1, вывести на экран сумму и завершить программу.  -1 должно учитываться в сумме.
*/
public class Solution
{

    public static void main(String[] args)   throws Exception
    {
        //напишите тут ваш код
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

//        System.out.println();
        int summ = 0;

        while(true){
            String s = bufferedReader.readLine();
            //System.out.println("String s is " + s);
            int f = Integer.parseInt(s);
            summ += f;
            if(f == -1.0f){
                System.out.println(summ);
                break;
            }
        }

    }
}

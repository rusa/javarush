package com.javarush.test.level04.lesson13.task02;

import java.io.*;

/* Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
8888
8888
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String a = bufferedReader.readLine();
        Integer m = Integer.parseInt(a);

        String b = bufferedReader.readLine();
        Integer n = Integer.parseInt(b);

        for (int itemM = m; itemM>0; itemM--){
            for (int itemN = n; itemN>0; itemN--){
                System.out.print('8');
            }
            System.out.println();
        }
    }
}

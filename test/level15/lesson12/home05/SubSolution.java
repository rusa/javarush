package com.javarush.test.level15.lesson12.home05;

/**
 * Created by rusamaha on 9/1/16.
 */
public class SubSolution extends Solution {
    public SubSolution(int i) {
    }
    protected SubSolution(float f) {
    }
    private SubSolution(String s) {
    }
    SubSolution(){}

    public SubSolution(Object o) {
    }
    protected SubSolution(char ch) {
    }
    private SubSolution(Double s) {
    }
    SubSolution(double d){}

    public SubSolution(long l) {
    }
    protected SubSolution(boolean bool) {
    }
    private SubSolution(byte b) {
    }
    SubSolution(short sh){}
}

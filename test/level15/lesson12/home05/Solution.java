package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

public class Solution {
    public Solution(int i) {
    }
    protected Solution(float f) {
    }
    private Solution(String s) {
    }
    Solution(){}

    public Solution(Object o) {
    }
    protected Solution(char ch) {
    }
    private Solution(Double s) {
    }
    Solution(double d){}

    public Solution(long l) {
    }
    protected Solution(boolean bool) {
    }
    private Solution(byte b) {
    }
    Solution(short sh){}
}


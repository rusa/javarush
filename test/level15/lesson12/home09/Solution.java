package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s;
        int indx;
        Map<String, String> map = new HashMap<String, String>();
        try {
            s = reader.readLine();
            indx = s.indexOf('?');
            s = s.substring(indx+1);
            String[] keyVal = s.split("&");

            for(String str: keyVal){
                if (str.contains("=")){
                    String[] pair = str.split("=");
                    map.put(pair[0], pair[1]);
                    System.out.print(pair[0]+ " ");
                } else {
                    map.put(str, null);
                    System.out.print(str + " ");
                }
            }

            for(Map.Entry<String, String> str: map.entrySet()){
                if(str.getKey().equals("obj")){
                    try {
                        System.out.println();
                        alert(Double.parseDouble(str.getValue()));

                    } catch (Exception ex){
                        System.out.println();
                        alert(str.getValue());
                    }
                }
            }

        } catch (Exception ex){

        }
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}

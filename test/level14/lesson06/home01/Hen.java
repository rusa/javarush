package com.javarush.test.level14.lesson06.home01;

/**
 * Created by rusamaha on 7/18/16.
 */
public abstract class Hen {
    abstract int getCountOfEggsPerMonth();
    String getDescription(){
        return "Я курица.";
    };
}

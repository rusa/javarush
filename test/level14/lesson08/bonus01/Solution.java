package com.javarush.test.level14.lesson08.bonus01;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //Add your code here
        try {
            Integer s = Integer.parseInt("фыв1");
        } catch (Exception e){
            exceptions.add(e);
        }

        try {
            Object o = new String("ads");
            Integer s = (Integer) o;
        } catch (Exception e){
            exceptions.add(e);
        }

        try {
            InputStream is = new FileInputStream("111.txt");
        } catch (Exception e){
            exceptions.add(e);
        }

        try {
            int[] arr = new int[3];
            arr[4] = 4;
        } catch (Exception e){
            exceptions.add(e);
        }

        try {
            Object[] o = new Double[5];
            o[0] = new Float(10);
        } catch (Exception e){
            exceptions.add(e);
        }

        try {
            int[] arr = new int[-3];

        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            ArrayList list = new ArrayList();
            list = null;
            list.add(1);
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            String s = new String("Test");
            s.charAt(10);
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            LinkedList list = new LinkedList();
            list.get(-1);
        } catch (Exception e) {
            exceptions.add(e);
        }

    }
}

package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by rusamaha on 8/1/16.
 */
public class Singleton {
    private static final Singleton s = new Singleton();

    private Singleton () {}

    static public Singleton getInstance(){
        return s;
    }
}

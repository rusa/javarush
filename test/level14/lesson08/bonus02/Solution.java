package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());

        if (a < b){
            int nod = a;
            while (true){
                if (b % nod == 0 && a % nod == 0) {
                        System.out.println(nod);
                        break;
                    } else {
                        nod--;
                    }
                }
            } else {
            int nod = b;
            while (true){
                if (a % nod == 0 && b % nod == 0) {
                    System.out.println(nod);
                    break;
                } else {
                    nod--;
                }
            }
        }
    }
}

package com.javarush.test.level14.lesson08.home01;

/**
 * Created by rusamaha on 7/18/16.
 */
public class SuspensionBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 30;
    }
}

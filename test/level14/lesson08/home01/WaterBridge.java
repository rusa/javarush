package com.javarush.test.level14.lesson08.home01;

/**
 * Created by rusamaha on 7/18/16.
 */
public class WaterBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 10;
    }
}

package com.javarush.test.level14.lesson08.home01;

/**
 * Created by rusamaha on 7/18/16.
 */
public interface Bridge {
    int getCarsCount();
}

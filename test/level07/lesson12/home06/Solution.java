package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        Human Zahid = new Human("Zahid", true, 89);
        Human Ilyas = new Human("Ilyas", true, 91);
        Human HavaA = new Human("HavaA", false, 90);
        Human HavaT = new Human("HavaT", false, 87);

        Human Alina = new Human("Alina", false, 26);
        Human Rustam = new Human("Rustam", true, 33);
        Human Rusa = new Human("Rusa", true, 30);

        Human Ildar = new Human("Ildar", true, 61);
        Human Lyutsia = new Human("Lyutsia", false, 62);

        Ildar.father = Zahid;
        Ildar.mother = HavaT;

        Lyutsia.father = Ilyas;
        Lyutsia.mother = HavaA;

        Rusa.father = Ildar;
        Rusa.mother = Lyutsia;

        Rustam.father = Ildar;
        Rustam.mother = Lyutsia;

        Alina.father = Ildar;
        Alina.mother = Lyutsia;

        System.out.println(Ilyas.toString());
        System.out.println(Zahid.toString());
        System.out.println(HavaA.toString());
        System.out.println(HavaT.toString());

        System.out.println(Ildar.toString());
        System.out.println(Lyutsia.toString());

        System.out.println(Alina.toString());
        System.out.println(Rusa.toString());
        System.out.println(Rustam.toString());

    }


    public static class Human
    {
        //напишите тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public Human father;
        public Human mother;

        Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}

package com.javarush.test.level07.lesson06.task03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

/* Самая короткая строка
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в список.
3. Используя цикл, найди самую короткую строку в списке.
4. Выведи найденную строку на экран.
5. Если таких строк несколько, выведи каждую с новой строки.
*/
public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        ArrayList<String> list = new ArrayList<String>();

        Reader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        for (int i = 0; i < 5; i++) {
            list.add(reader.readLine());
        }

        ArrayList<String> listShortest = new ArrayList<String>();

        String shortest = list.get(0);
        listShortest.add(shortest);

        for (int i = 1; i < 5; i++) {
            if (list.get(i).length() < shortest.length()) {
                shortest = list.get(i);
                listShortest.clear();
                listShortest.add(shortest);
            } else if (list.get(i).length() == shortest.length()) {
                listShortest.add(list.get(i));
            }
        }
        for (int i = 0; i < listShortest.size(); i++){
            System.out.println(listShortest.get(i));
        }

    }
}

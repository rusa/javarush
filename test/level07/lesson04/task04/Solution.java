package com.javarush.test.level07.lesson04.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

/* Массив из чисел в обратном порядке
1. Создать массив на 10 чисел.
2. Ввести с клавиатуры 10 чисел и записать их в массив.
3. Расположить элементы массива в обратном порядке.
4. Вывести результат на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        Integer[] nums = new Integer[10];

        Reader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        for(int i=0; i<10; i++){
            nums[i] = Integer.parseInt(reader.readLine());
        }
        int c = 9;
        for(int i=0; i<5; i++){
            int temp = nums[i];
            nums[i] = nums[c];
            nums[c] = temp;
            c--;


        }
        for (int i = 0; i<10; i++){
            System.out.println(nums[i]);
        }
    }
}

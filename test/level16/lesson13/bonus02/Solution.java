package com.javarush.test.level16.lesson13.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* Клубок
1. Создай 5 различных своих нитей c отличным от Thread типом:
1.1. нить 1 должна бесконечно выполняться;
1.2. нить 2 должна выводить "InterruptedException" при возникновении исключения InterruptedException;
1.3. нить 3 должна каждые полсекунды выводить "Ура";
1.4. нить 4 должна реализовать интерфейс Message, при вызове метода showWarning нить должна останавливаться;
1.5. нить 5 должна читать с консоли цифры пока не введено слово "N", а потом вывести в консоль сумму введенных цифр.
2. В статическом блоке добавь свои нити в List<Thread> threads в перечисленном порядке.
3. Нити не должны стартовать автоматически.
Подсказка: Нить 4 можно проверить методом isAlive()
*/

public class Solution {

    public static void main(String[] args) throws InterruptedException {

        Thread messageThread = threads.get(3);
        messageThread.start();
        Message message = (Message) messageThread;
        message.showWarning();
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println(messageThread.isAlive());
        System.out.println(threads.get(3).isAlive());
    }
    public static List<Thread> threads = new ArrayList<Thread>(5);
    static {
        threads.add(new InfinityThread1());
        threads.add(new InterupExepThread2());
        threads.add(new HoorayThread3());
        threads.add(new MessageThread4());
        threads.add(new NThread5());
    }
    public static class InfinityThread1 extends Thread {
        @Override
        public void run() {
            while(true){

            }
        }
    }
    public static class InterupExepThread2 extends Thread {
        @Override
        public void run() {
            try {
                while (true){
                    sleep(1000);
                }
            }catch (InterruptedException ex){
                System.out.println("InterruptedException");
            }
        }
    }
    public static class HoorayThread3 extends Thread {
        @Override
        public void run() {
            try {
                while (true){
                    System.out.println("Ура");
                    sleep(500);
                }
            }catch (InterruptedException ex){

            }
        }
    }
    public static class MessageThread4 extends Thread implements Message {

        @Override
        public void showWarning() {
            interrupt();
            try {
                join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (!isInterrupted()){

            }
        }
    }
    public static class NThread5 extends Thread {

        @Override
        public void run() {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String s;
            Double d = 0.0;
            try {
                while (true){
                    s = bufferedReader.readLine();
                    if(s.equals("N")){
                        bufferedReader.close();
                        break;
                    }
                    d+=Integer.parseInt(s);
                }
                System.out.println(d);
            } catch (IOException e) {

            }
        }
    }
}

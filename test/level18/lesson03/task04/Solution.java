package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        FileInputStream fileInputStream = new FileInputStream(fileName);

        Map<Integer, Integer> integerIntegerMap = new HashMap<>();
        int tempByte;
        int min = 1;
        while (fileInputStream.available()>0){

            tempByte = fileInputStream.read();

            if(integerIntegerMap.containsKey(tempByte)){
                integerIntegerMap.put(tempByte, integerIntegerMap.get(tempByte)+1);
            } else {
                integerIntegerMap.put(tempByte, 1);
            }
            int count = integerIntegerMap.get(tempByte);
            if(count<min){
                min = count;
            }

        }
//        integerIntegerMap.entrySet();
        for(Map.Entry<Integer, Integer> entry: integerIntegerMap.entrySet()){
            if(entry.getValue() == min){
                System.out.print(entry.getKey()+" ");
            }
        }
        System.out.println(integerIntegerMap);
        System.out.println(min);

        bufferedReader.close();
        fileInputStream.close();


    }
}

package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;




/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        FileInputStream fileInputStream = new FileInputStream(fileName);

        List<Integer> list = new ArrayList<>();

        while (fileInputStream.available() > 0) {
            int temp = fileInputStream.read();
            if(!list.contains(temp)){
                list.add(temp);
            }
        }

        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) > list.get(j)) {
                    int k = list.get(j);
                    list.set(j, list.get(i));
                    list.set(i, k);
                }
            }
        }

        for (int x: list){
            System.out.print(x+ " ");
        }
        System.out.println();
        System.out.println("////////////////////////////");
        System.out.println(list);

        fileName = bufferedReader.readLine();
        fileInputStream = new FileInputStream(fileName);


        Set<Integer> set = new TreeSet<>();

        while (fileInputStream.available() > 0) {
            set.add(fileInputStream.read());
        }
        System.out.println(set);


        bufferedReader.close();
        fileInputStream.close();
    }
}

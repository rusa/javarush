package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        bufferedReader.close();

        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

        ArrayList<String> tempList = new ArrayList<>();

        String productName = "";
        for (int i = 2; i < args.length - 2; i++) {
            productName += args[i] + " ";
        }

        String price = args[args.length - 2];
        String quantity = args[args.length - 1];

        int searchID = Integer.valueOf(args[1]);

        while (fileReader.ready()) {
            String line = fileReader.readLine();
            if (line.length() == 0) {
                tempList.add(line);
                continue;
            }
            int tempID = (Integer.parseInt((line.substring(0, 8)).trim()));
            if (tempID == searchID) {
                if (args[0].equals("-u")) {
                    String s = String.format("%-8d%-30.30s%-8.2f%-4d", tempID, productName, Float.valueOf(price), Integer.valueOf(quantity));
                    System.out.println(s);
                    tempList.add(s);
                } else if (args[0].equals("-d")) {

                }
            } else {
                tempList.add(line);
            }
        }
        System.out.println(tempList);
        FileWriter fileWriter = new FileWriter(fileName);

        for (String s : tempList) {
            fileWriter.write(String.format("%s%n", s));
        }


        fileWriter.close();
        fileReader.close();


    }
}

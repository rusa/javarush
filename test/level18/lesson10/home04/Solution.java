package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();

        RandomAccessFile randomAccessFile = new RandomAccessFile(fileName1, "rw");

        byte[] array = new byte[(int) randomAccessFile.length()];
        randomAccessFile.read(array);
        randomAccessFile.seek(0);

        FileInputStream fileInputStream = new FileInputStream(fileName2);

        while (fileInputStream.available()>0) {
            randomAccessFile.write(fileInputStream.read());
        }
        randomAccessFile.write(array);

        bufferedReader.close();
        fileInputStream.close();
        randomAccessFile.close();


//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName1 = bufferedReader.readLine();
//        String fileName2 = bufferedReader.readLine();
//        bufferedReader.close();
//
//        FileInputStream fileInputStream1 = new FileInputStream(fileName1);
//        FileInputStream fileInputStream2 = new FileInputStream(fileName2);
//
//        byte[] buffer1 = new byte[fileInputStream1.available()];
//        fileInputStream1.read(buffer1);
//        fileInputStream1.close();
//
//        byte[] buffer2 = new byte[fileInputStream2.available()];
//        fileInputStream2.read(buffer2);
//        fileInputStream2.close();
//
//        FileOutputStream fileOutputStream = new FileOutputStream(fileName1);
//        fileOutputStream.write(buffer2);
//        fileOutputStream.close();
//
//        fileOutputStream = new FileOutputStream(fileName1, true);
//        fileOutputStream.write(buffer1);
//        fileOutputStream.close();
    }
}

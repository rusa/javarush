package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать в первый файл содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();
        String fileName3 = bufferedReader.readLine();
        bufferedReader.close();

        FileInputStream fileInputStream2 =  new FileInputStream(fileName2);
        FileInputStream fileInputStream3 =  new FileInputStream(fileName3);
        FileOutputStream fileOutputStream = new FileOutputStream(fileName1);

        byte[] buffer2 = new byte[fileInputStream2.available()];
        fileInputStream2.read(buffer2);


        byte[] buffer3 = new byte[fileInputStream3.available()];
        fileInputStream3.read(buffer3);

        fileOutputStream.write(buffer2);
        fileOutputStream.write(buffer3);

        fileInputStream2.close();
        fileInputStream3.close();
        fileOutputStream.close();


//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName1 = bufferedReader.readLine();
//        String fileName2 = bufferedReader.readLine();
//        String fileName3 = bufferedReader.readLine();
//        bufferedReader.close();
//
//        FileInputStream fileInputStream;
//        FileOutputStream fileOutputStream;
//
//        StringBuilder sb = new StringBuilder();
//
//        fileInputStream = new FileInputStream(fileName2);
//        while (fileInputStream.available()>0){
//            sb.append((char)fileInputStream.read());
//        }
//
//        fileInputStream = new FileInputStream(fileName3);
//        while (fileInputStream.available()>0){
//            sb.append((char)fileInputStream.read());
//        }
//
//        InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
//        fileOutputStream = new FileOutputStream(fileName1);
//        while (is.available()>0){
//            fileOutputStream.write(is.read());
//        }
//
//        is.close();
//
//        fileInputStream.close();
//        fileOutputStream.close();
    }
}

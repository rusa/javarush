package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки. Не использовать try-with-resources

Пример вывода:
, 19
- 7
f 361
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        FileInputStream fileInputStream = new FileInputStream(fileName);
        Map<Integer, Integer> integerIntegerMap = new HashMap<>();
        int tempByte;

        while (fileInputStream.available()>0){
            tempByte = fileInputStream.read();
            if(integerIntegerMap.containsKey(tempByte)){
                integerIntegerMap.put(tempByte, integerIntegerMap.get(tempByte)+1);
            } else {
                integerIntegerMap.put(tempByte, 1);
            }
        }

        ArrayList<Integer> arrayList = new ArrayList<>();
        for(Map.Entry<Integer, Integer> entry: integerIntegerMap.entrySet()){
            arrayList.add(entry.getKey());
        }

        sort(arrayList);
        for (Integer val: arrayList){
            System.out.println((char) (int)val + " " + integerIntegerMap.get(val));
        }

        System.out.println(integerIntegerMap);

        fileInputStream.close();
    }

    private static void sort(ArrayList<Integer> aList){

        for(int i=0; i<aList.size()-1; i++) {
            for (int j = i; j < aList.size(); j++) {
                if (aList.get(j) < aList.get(i)) {
                    Integer buffer = aList.get(i);
                    aList.set(i, aList.get(j));
                    aList.set(j, buffer);
                }
            }
        }
    }
}

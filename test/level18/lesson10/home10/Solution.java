package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> partsNameArr = new ArrayList<>();
        while (true){
            String filePartName = bufferedReader.readLine();
            if(filePartName.equals("end")) break;
            partsNameArr.add(filePartName);
        }

        String[] resultArray = partsNameArr.get(0).split(".part");

        File randomAccessFile = new File(resultArray[0]);

        FileOutputStream fileOutputStream = new FileOutputStream(randomAccessFile, true);

        sortFiles(partsNameArr);

        for(String part: partsNameArr){

            FileInputStream fileInputStream = new FileInputStream(part);

            int fileSize = fileInputStream.available();
            byte[] buffer = new byte[fileSize];
            while (fileInputStream.available() > 0) {
                fileInputStream.read(buffer);
                fileOutputStream.write(buffer);
            }
            fileInputStream.close();
        }

        bufferedReader.close();

        fileOutputStream.close();
    }

    private static void sortFiles(ArrayList<String> filesList){

        for(int i=0; i<filesList.size()-1; i++) {
            for (int j = i; j < filesList.size(); j++) {
                if (Integer.parseInt((filesList.get(j).split(".part"))[1]) < Integer.parseInt(((filesList.get(i).split(".part"))[1]))) {
                    String buffer = filesList.get(i);
                    filesList.set(i, filesList.get(j));
                    filesList.set(j, buffer);
                }
            }
        }
    }
}

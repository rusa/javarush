package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];
        FileInputStream fileInputStream = new FileInputStream(fileName);
        int count = 0;
        while (fileInputStream.available() > 0) {
            // Variant 1
            int ch = fileInputStream.read();
            if((ch>64 && ch<91) || (ch>96 && ch <123)){
                count++;
            }
            // Variant 2
            //String s = String.valueOf((char)fileInputStream.read());
            //if(s.matches("[a-zA-Z]{1}")){
            //count++;
            //}
        }
        fileInputStream.close();
        System.out.println(count);
    }
}


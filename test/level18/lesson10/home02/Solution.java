package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];
//        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
//        bufferedReader.readLine();

        FileInputStream fileInputStream = new FileInputStream(fileName);
        int countAll = 0;
        int countSpace = 0;
        double res = 0;
        while (fileInputStream.available()>0){
            int current =fileInputStream.read();
            countAll++;
            if(current==32){
                countSpace++;
            }
        }
        res = (double)countSpace/countAll*100;
        System.out.printf("%.2f", res);

        fileInputStream.close();
    }
}

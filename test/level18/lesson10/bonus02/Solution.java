package com.javarush.test.level18.lesson10.bonus02;

/* Прайсы
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается со следующим набором параметров:
-c productName price quantity
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-c  - добавляет товар с заданными параметрами в конец файла, генерирует id самостоятельно, инкрементируя максимальный id, найденный в файле

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        bufferedReader.close();
        File file = new File(fileName);

        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

        FileWriter fileWriter = new FileWriter(fileName, true);

//        if(args[0].equals("-c")) {
            String productName = "";
            for(int i = 1; i<args.length-2; i++){
                productName += args[i]+" ";
            }

            String price = args[args.length-2];
            String quantity = args[args.length-1];

            int maxID = 0;

            while (fileReader.ready()) {
                String line = fileReader.readLine();
                if(line.length() == 0) continue;
                int tempID = (Integer.parseInt((line.substring(0,8)).trim()));
                if(tempID > maxID) {
                    maxID = tempID;
                }
            }

            if(file.length()>0){
                fileWriter.write(String.format("%n%-8d%-30.30s%-8.2f%-4d",1+maxID,productName,Float.valueOf(price),Integer.valueOf(quantity)));
            } else {
                fileWriter.write(String.format("%n%-8d%-30.30s%-8.2f%-4d", 1, productName, Float.valueOf(price), Integer.valueOf(quantity)));
            }

            fileWriter.close();
            fileReader.close();
//        }
    }
}

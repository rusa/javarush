package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while (true) {
            s = bufferedReader.readLine();
            if (s.equals("exit")) {
                break;
            }

            Thread thread = new ReadThread(s);
            thread.start();
        }

        System.out.println("ResultMap " + resultMap);
        bufferedReader.close();
    }

    public static class ReadThread extends Thread {
        private String fileName;

        public ReadThread(String fileName) throws FileNotFoundException {
            //implement constructor body
            this.fileName = fileName;

        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run() {
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(fileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Map<Integer, Integer> integerIntegerMap = new HashMap<>();
            int tempByte;
            int tempByteMax = 0;
            int max=0;
            try {
                while (fileInputStream.available() > 0) {
                    tempByte = fileInputStream.read();
                    if (integerIntegerMap.containsKey(tempByte)) {
                        integerIntegerMap.put(tempByte, integerIntegerMap.get(tempByte) + 1);
                    } else {
                        integerIntegerMap.put(tempByte, 1);
                    }
                    if(integerIntegerMap.get(tempByte) > max){
                        max = integerIntegerMap.get(tempByte);
                        tempByteMax=tempByte;
                    }


                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("max ASCII symbol "+ max);
            System.out.println("IntegerIntegerMap " + integerIntegerMap);
            resultMap.put(fileName, tempByteMax);
        }
    }
}

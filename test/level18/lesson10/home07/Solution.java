package com.javarush.test.level18.lesson10.home07;

/* Поиск данных внутри файла
Считать с консоли имя файла
Найти в файле информацию, которая относится к заданному id, и вывести ее на экран в виде, в котором она записана в файле.
Программа запускается с одним параметром: id (int)
Закрыть потоки. Не использовать try-with-resources

В файле данные разделены пробелом и хранятся в следующей последовательности:
id productName price quantity

где id - int
productName - название товара, может содержать пробелы, String
price - цена, double
quantity - количество, int

Информация по каждому товару хранится в отдельной строке
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {

        Integer id = Integer.parseInt(args[0]);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        FileInputStream fileInputStream = new FileInputStream(fileName);

        Scanner scanner = new Scanner(fileInputStream);

        String s;
        String[] strArr;

        while (scanner.hasNextLine()){
            s = scanner.nextLine();
            strArr = s.split(" ");
            if(Integer.parseInt(strArr[0]) == id){
                System.out.println(s);
                break;
            }
        }
        bufferedReader.close();
        fileInputStream.close();
        scanner.close();
    }
}

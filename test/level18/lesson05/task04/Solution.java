package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = bufferedReader.readLine();
        String file2 = bufferedReader.readLine();

        FileInputStream fileInputStream = new FileInputStream(file1);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);

        int count = fileInputStream.available();
        byte[] bufer = new byte[count];

        fileInputStream.read(bufer);

        for (int i = bufer.length-1; i>=0; i--){
            fileOutputStream.write(bufer[i]);
        }

        bufferedReader.close();
        fileInputStream.close();
        fileOutputStream.close();

    }
}

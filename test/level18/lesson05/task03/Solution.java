package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = bufferedReader.readLine();
        String file2 = bufferedReader.readLine();
        String file3 = bufferedReader.readLine();
        FileInputStream fileInputStream = new FileInputStream(file1);
        FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
        FileOutputStream fileOutputStream3 = new FileOutputStream(file3);

        int fileSize1 = fileInputStream.available();

        int fileSize3 = fileSize1 / 2;
        int fileSize2 = fileSize1 - fileSize3;

        byte[] buffer2 = new byte[fileSize2];
        byte[] buffer3 = new byte[fileSize3];

        fileInputStream.read(buffer2);
        fileInputStream.read(buffer3);

        fileOutputStream2.write(buffer2, 0, fileSize2);
        fileOutputStream3.write(buffer3, 0, fileSize3);
        bufferedReader.close();
        fileInputStream.close();
        fileOutputStream2.close();
        fileOutputStream3.close();

    }
}

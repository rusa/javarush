package com.javarush.test.level05.lesson12.home04;

/* Вывести на экран сегодняшнюю дату
Вывести на экран текущую дату в аналогичном виде "21 02 2014".
*/

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код

        Date dateNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("dd MM yyyy");

        System.out.println(ft.format(dateNow));

    }
}

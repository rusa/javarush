package com.javarush.test.level05.lesson12.home02;

/* Man and Woman
1. Внутри класса Solution создай public static классы Man и Woman.
2. У классов должны быть поля: name(String), age(int), address(String).
3. Создай конструкторы, в которые передаются все возможные параметры.
4. Создай по два объекта каждого класса со всеми данными используя конструктор.
5. Объекты выведи на экран в таком формате [name + " " + age + " " + address].
*/

public class Solution {
    public static void main(String[] args) {
        //создай по два объекта каждого класса тут
        Man man1 = new Man("Rusa", 33, "California");
        Man man2 = new Man("Petya", 21, "New York");

        Woman woman1 = new Woman("Maha", 25, "California");
        Woman woman2 = new Woman("Shura", 20, "Quebec");
        //выведи их на экран тут
        System.out.println(man1);
        System.out.println(man2);

        System.out.println(woman1);
        System.out.println(woman2);

    }

    //добавьте тут ваши классы
    public static class Man {
        private String name;
        private int age;
        private String address;

        public Man (){

        }
        public Man (String name){
            this.name = name;
        }
        public Man (String name, int age, String address){
            this.name = name;
            this.address = address;
            this.age = age;
        }
        public Man (int age, String address){
            this.name = "noname";
            this.address = address;
            this.age = age;
        }
        public Man (String name, int age){
            this.name = name;
            this.address = "BOMJ";
            this.age = age;
        }
        public String toString(){
            return this.name + " " + this.age + " " + this.address;
        }
    }
    public static class Woman {
        private String name;
        private int age;
        private String address;

        public Woman (){

        }
        public Woman (String name){
            this.name = name;
        }
        public Woman (String name, int age, String address){
            this.name = name;
            this.address = address;
            this.age = age;
        }
        public Woman (int age, String address){
            this.name = "noname";
            this.address = address;
            this.age = age;
        }
        public Woman (String name, int age){
            this.name = name;
            this.address = "BOMJ";
            this.age = age;
        }
        public String toString(){
            return this.name + " " + this.age + " " + this.address;
        }


    }

}

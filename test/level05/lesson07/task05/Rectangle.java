package com.javarush.test.level05.lesson07.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше методов initialize(…)
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    //напишите тут ваш код
    private Integer top, left, width, height;

    public void initialize(Integer left, Integer top, Integer width, Integer height){
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;

    }
    public void initialize(){
        this.left = 0;
        this.top = 0;
        this.width = 0;
        this.height = 0;

    }
    public void initialize(Integer left, Integer top){
        this.left = left;
        this.top = top;
        this.width = 0;
        this.height = 0;

    }
    public void initialize(Integer left, Integer top, Integer width){
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = width;

    }
    public void initialize(Rectangle r){
        this.left = r.left;
        this.top = r.top;
        this.width = r.width;
        this.height = r.height;

    }
}

package com.javarush.test.level12.lesson06.task05;

/* Классы Cat и Dog от Pet
Унаследуй классы Cat и Dog от Pet.
Реализуй недостающие методы. Классы Cat и Dog не должны быть абстрактными.
*/

public class Solution
{
    public static void main(String[] args)
    {

        Object o = new Cat("Tom");
        System.out.println(o.getClass());
        System.out.println(o.toString());
        Cat c = (Cat) o;
        c.sound();
        System.out.println(c.name);
    }

    public static abstract class Pet
    {
        public abstract String getName();
        public abstract Pet getChild();
    }

    public static class Cat extends Pet
    {
        String name;
        public Cat(String n){
            this.name = n;
        }
        public Pet getChild() {
            return null;
        }
        public String getName() {
            return null;
        }
        public String sound(){
            System.out.println("Lalal");
            return "Miu";
        }
    }

    public static class Dog extends Pet
    {
        public Pet getChild() {
            return null;
        }
        public String getName() {
            return null;
        }
    }

}

package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        //напишите тут ваш код

        Set<Integer> integerSet = new HashSet<Integer>();

        for (int i = 0; i<20; i++) {
//            Integer s = (int)(Math.random()*100);
            integerSet.add(i);
        }
        return (HashSet)integerSet;
    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        //напишите тут ваш код
        HashSet<Integer> tempIntegerHashSet = new HashSet<Integer>();
        for(Integer i: set){
            if(i>10){
                tempIntegerHashSet.add(i);

            }
        }
        set.removeAll(tempIntegerHashSet);
        for(Integer a: set){
            System.out.println(a);
        }
        return set;
    }

    public static void main(String[] args){
        createSet();
        removeAllNumbersMoreThan10(createSet());
    }
}

package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        //напишите тут ваш код
        Set<String> stringSet = new HashSet<String>();

        for (int i = 0; i<20; i++){
            String s = "Л" + Math.random();
            stringSet.add(s);
            System.out.println(s);
        }
        return (HashSet)stringSet;

    }
}

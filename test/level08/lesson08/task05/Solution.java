package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static void main(String[] args) {
        HashMap<String, String> myMap = createMap();
        removeTheFirstNameDuplicates(myMap);
        for (Map.Entry<String, String> pair: myMap.entrySet()) {
            System.out.println(pair);
        }
    }
    public static HashMap<String, String> createMap()
    {
        //напишите тут ваш код
        HashMap<String, String> names = new HashMap<String, String>();
        names.put("Petrov","Vasya");
        names.put("Ivanov","Vasya");
        names.put("Guvanov","Kolya");
        names.put("Adams","John");
        names.put("Kozlov","Vasya");
        names.put("Papanov","Vasya");
        names.put("Zvanov","Vasya");
        names.put("Baganov","Petya");
        names.put("Dronov","Vasya");
        names.put("Jivanov","Petya");
        return names;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        //напишите тут ваш код
        HashMap<String, String> copy = new HashMap<String, String>(map);
        HashMap<String, Integer> duplicateNames = new HashMap<String, Integer>();
        for(Map.Entry<String, String> pair: copy.entrySet()){
            String name = pair.getValue();
            if (duplicateNames.size() == 0 || !duplicateNames.containsKey(name)){
                duplicateNames.put(name, 1);
            } else if (duplicateNames.containsKey(name)){
                duplicateNames.put(name, duplicateNames.get(name)+1);
            }
        }

        for(Map.Entry<String, Integer> pair: duplicateNames.entrySet()){
            if(pair.getValue()>1) {
                removeItemFromMapByValue(map, pair.getKey());
            }
        }

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}

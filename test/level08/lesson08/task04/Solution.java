package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", new Date("JUNE 1 1980"));

        //напишите тут ваш код
        map.put("Alina", new Date("APRIL 22 1990"));
        map.put("Father", new Date("APRIL 7 1954"));
        map.put("Mother", new Date("June 9 1954"));
        map.put("Me", new Date("June 7 1983"));
        map.put("Maha", new Date("JULY 26 1991"));
        map.put("Richa", new Date("JANUARY 1 1996"));
        map.put("Aby", new Date("DECEMBER 30 1925"));
        map.put("Zeman", new Date("NOVEMBER 10 1983"));
        map.put("Urs", new Date("MARCH 21 1981"));

        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        for(Iterator<Map.Entry<String, Date>> it = map.entrySet().iterator(); it.hasNext();){
            Map.Entry<String, Date> entry = it.next();
            if (entry.getValue().getMonth() > 4 && entry.getValue().getMonth() < 8) {
                it.remove();
            }
        }
    }
}

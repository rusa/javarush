package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //напишите тут ваш код
        Map<String, String> surnameName = new HashMap<String, String>();
            surnameName.put("Пупкин", "Вася");
            surnameName.put("Смактуновский", "Петя");
            surnameName.put("Болванц", "Иван");
            surnameName.put("Гаргулья", "Евгеньевна");
            surnameName.put("Петунья", "Калдунья");
            surnameName.put("Смактуновскийа", "Вася");
            surnameName.put("Пупкинц", "Петя");
            surnameName.put("Гаргульяц", "Калдунья");
            surnameName.put("Пупкинэ", "Петя");
            surnameName.put("Болван", "Митрофан");

        return (HashMap)surnameName;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        //напишите тут ваш код
        int count = 0;
        for(Map.Entry<String, String> pair: map.entrySet()){
            if(name.equals(pair.getValue())){
                count++;
            }
        }
        return count;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName)
    {
        //напишите тут ваш код
        int count = 0;
        for(Map.Entry<String, String> pair: map.entrySet()){
            if(lastName.equals(pair.getKey())){
                count++;
            }
        }
        return count;

    }
    public static void main(String[] args) {
        System.out.println(getCountTheSameFirstName(createMap(), "Петя"));
        System.out.println(getCountTheSameFirstName(createMap(), "Калдунья"));

    }

}

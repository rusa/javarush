package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        Human child1 = new Human("Petya", true, 10);
        System.out.println(child1);

        Human child2 = new Human("Vasya", true, 12);
        System.out.println(child2);

        Human child3 = new Human("Monya", false, 8);
        System.out.println(child3);

        Human mother = new Human("Mamy", false, 35);
        mother.setChildren(child1);
        mother.setChildren(child2);
        mother.setChildren(child3);
        System.out.println(mother);

        Human father = new Human("Dady", true, 40);
        father.setChildren(child1);
        father.setChildren(child2);
        father.setChildren(child3);
        System.out.println(father);

        Human gMother1 = new Human("GMother1", false, 70);
        gMother1.setChildren(mother);
        System.out.println(gMother1);

        Human gFather1 = new Human("GFather1", true, 75);
        gFather1.setChildren(gFather1);
        System.out.println(gFather1);

        Human gMother2 = new Human("GMother2", false, 80);
        gMother1.setChildren(father);
        System.out.println(gMother2);

        Human gFather2 = new Human("GFather2", true, 85);
        gFather1.setChildren(father);
        System.out.println(gFather2);
    }

    public static class Human
    {
        //напишите тут ваш код
        private String name;
        private boolean sex;
        private int age;
        private ArrayList<Human> children;

        Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = new ArrayList<Human>();

        }
        public void setChildren(Human children){

            this.children.add(children);
        }
        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}

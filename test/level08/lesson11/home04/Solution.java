package com.javarush.test.level08.lesson11.home04;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* Минимальное из N чисел
1. Ввести с клавиатуры число N.
2. Считать N целых чисел и заполнить ими список - метод getIntegerList.
3. Найти минимальное число среди элементов списка - метод getMinimum.
*/

public class Solution
{
    public static void main(String[] args) throws Exception {
        List<Integer> integerList = getIntegerList();
        System.out.println(getMinimum(integerList));

    }

    public static int getMinimum(List<Integer> array) {
        // Найти минимум
        Integer min = array.get(1);
        for(Integer i : array){
            if(i < min){
                min = i;
            }
        }

        return min;
    }

    public static List<Integer> getIntegerList() throws IOException {
        //Тут создать и заполнить список
        List<Integer> integerList = new ArrayList<Integer>();

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        Integer N = Integer.parseInt(bufferedReader.readLine());

        for (int i = 0; i < N; i++){
            integerList.add(Integer.parseInt(bufferedReader.readLine()));
        }

        return integerList;
    }
}

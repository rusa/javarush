package com.javarush.test.level08.lesson11.home09;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args){
        int a = 10;
        int b = 100;
        double c =(double) (a/b);
        String str =String.format("%1.4f", c);
        System.out.println(str);
    }

    public static boolean isDateOdd(String date) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd yyyy");
        String dateInString = date;

        Date dateIs = formatter.parse(dateInString);; // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateIs);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        //System.out.println(year + " | " + month + " | " + day );

        Date date1 = formatter.parse(dateInString);
        Date date2 = formatter.parse("JANUARY 0 " + year);

        long dif = date1.getTime() - date2.getTime();

        long isOdd = (dif/(24 * 60 * 60 * 1000))%2;
        if(isOdd == 1) {
            return true;
        } else {
            return false;
        }
    }
}

package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.*;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
Используйте коллекции.
*/

public class Solution
{
    public static void main(String[] args) throws IOException, ParseException {
        //напишите тут ваш код
        //List<String> months = Arrays.asList("January","February","March","April","May","June","July","August","September","October","November","December");
        ArrayList<String> months = new ArrayList<String>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");

        Map<String, String> monthsMap = new HashMap<String, String>();
        //String[] months = new DateFormatSymbols().getMonths();
        for(int i =0; i< months.size(); i++){
            monthsMap.put(months.get(i), i+1+"");
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String dateInString = reader.readLine();

        System.out.println(dateInString + " is " + monthsMap.get(dateInString) + " month");

    }
}

package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import com.sun.org.apache.xpath.internal.operations.Mod;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String fileName = reader.readLine();

        BufferedReader inputStream = new BufferedReader((new InputStreamReader(new FileInputStream(fileName))));

        ArrayList<Integer> list = new ArrayList<Integer>();
        String line;
        while ( (line = inputStream.readLine())!=null){
            list.add(Integer.parseInt(line));
        }

        for(int i = 0; i < list.size(); i++){

            for(int j = i; j < list.size(); j++){
                if(list.get(j) < list.get(i)){
                    int buffer = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, buffer);
                }
            }
        }

        for(int x: list){
            if(x % 2 == 0){

            System.out.println(x);
            }
        }

    }
}

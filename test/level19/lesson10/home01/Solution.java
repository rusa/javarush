package com.javarush.test.level19.lesson10.home01;

/* Считаем зарплаты
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Все данные вывести в консоль, предварительно отсортировав в возрастающем порядке по имени
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 2
Сидоров 6
Иванов 1.35
Петров 3.1

Пример вывода:
Иванов 1.35
Петров 5.1
Сидоров 6.0
*/

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        FileReader fileReader = new FileReader(fileName);

        //Arthur realization Artur!!!

//        HashMap<String, Double> nameVal = new HashMap<>();
//
//        Scanner scanner = new Scanner(new FileInputStream(fileName));
//        while (scanner.hasNextLine()) {
//            String tokens[] = scanner.nextLine().split(" ");
//            String name = tokens[0];
//            Double value = nameVal.getOrDefault(name, 0.0);
//            nameVal.put(name, value + Double.parseDouble(tokens[1]));
//        }
//
//        nameVal.entrySet().stream()
//                .sorted(new Comparator<Map.Entry<String, Double>>() {
//                    @Override
//                    public int compare(Map.Entry<String, Double> a, Map.Entry<String, Double> b) {
//                        return a.getKey().compareTo(b.getKey());
//                    }
//                })
//                .sorted((a,b) -> a.getKey().compareTo(b.getKey()))
//                .forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));
//
//        scanner.close();

        HashMap<String, Double> nameVal = new HashMap<>();
        ArrayList<String> names = new ArrayList<>();

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String[] token;
        while (bufferedReader.ready()) {
            token = bufferedReader.readLine().split(" ");
            if (nameVal.containsKey(token[0])) {
                nameVal.put(token[0], nameVal.get(token[0]) + Double.parseDouble(token[1]));
            } else {
                nameVal.put(token[0], Double.parseDouble(token[1]));
                names.add(token[0]);
            }
        }
        sort(names);

        for(String s: names){
            System.out.println(s + " " + nameVal.get(s));
        }
    }

    public static void sort(ArrayList<String> aList) {
        for(int i = 0; i< aList.size(); i++){
            for(int j=i; j<aList.size(); j++){
                if(aList.get(i).compareTo(aList.get(j))>0){

                    String buffer = aList.get(i);
                    aList.set(i, aList.get(j));
                    aList.set(j, buffer);
                }
            }
        }
    }
}
package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        while (bufferedReader.ready()){
            String s = bufferedReader.readLine();
            String name = s.substring(0, s.lastIndexOf(' ', s.lastIndexOf(' ', s.lastIndexOf(' ')-1)-1));

            String[] line = s.split(" ");

//            String name = (new String[line.length-3]).toString();

            int year = Integer.parseInt(line[line.length-1]);
            int month = Integer.parseInt(line[line.length-2]);
            int day = Integer.parseInt(line[line.length-3]);
//            String name = "";
//            for (int i=0; i<line.length-3; i++){
//                 name += line[i]+" ";
//            }

            Calendar date = new GregorianCalendar(year,month-1,day);

            Person person = new Person(name, date.getTime());

            PEOPLE.add(person);
        }
        bufferedReader.close();

//        for (Person p: PEOPLE){
//            System.out.println(p.getName() + " " + p.getBirthday());
//        }
    }
}

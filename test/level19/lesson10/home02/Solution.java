package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        //Scanner scanner = new Scanner(new FileInputStream(fileName));

//        while (scanner.hasNextLine()) {
//            String tokens[] = scanner.nextLine().split(" ");
//            String name = tokens[0];
//            Double value = nameVal.getOrDefault(name, 0.0);
//            nameVal.put(name, value + Double.parseDouble(tokens[1]));
//        }

        HashMap<String, Double> nameVal = new HashMap<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

        String[] token;
        while (bufferedReader.ready()) {
            while (bufferedReader.ready()) {
                token = bufferedReader.readLine().split(" ");
                if (nameVal.containsKey(token[0])) {
                    nameVal.put(token[0], nameVal.get(token[0]) + Double.parseDouble(token[1]));
                } else {
                    nameVal.put(token[0], Double.parseDouble(token[1]));
                }
            }
        }

        Double max = 0.0;
//        nameVal.entrySet().stream().filter(e -> e.getValue().compareTo(max) > 0).forEach(e -> {
//            max = e.getValue();
//        });
///     We will not use stream and Lambda :( because we have old java 1.6 so... >>>>
        for (Map.Entry<String, Double> entry : nameVal.entrySet()) {
            if (entry.getValue().compareTo(max) > 0) {
                max = entry.getValue();
            }
        }

        for (Map.Entry<String, Double> entry : nameVal.entrySet()) {
            if (entry.getValue().equals(max)) {
                System.out.println(entry.getKey());
                bufferedReader.close();
            }
        }

        //for Java 1.8 >>>
//        try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
//
//            HashMap<String, Double> nameVal = new HashMap<>();
//            while (scanner.hasNextLine()) {
//                String tokens[] = scanner.nextLine().split(" ");
//                String name = tokens[0];
//                Double value = nameVal.getOrDefault(name, 0.0);
//                nameVal.put(name, value + Double.parseDouble(tokens[1]));
//            }
//
//            Optional<Map.Entry<String, Double>> result = nameVal.entrySet().stream()
//                    .max((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
//
//            Map.Entry<String, Double> entry = result.get();
//
//            nameVal.entrySet().stream().forEach(stringDoubleEntry -> {
//                if (stringDoubleEntry.getValue().equals(entry.getValue())) {
//                    System.out.println(stringDoubleEntry.getKey());
//                }
//            });
//        }

    }
}



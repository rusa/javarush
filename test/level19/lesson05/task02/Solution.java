package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();

        FileInputStream fileInputStream = new FileInputStream(fileName);

        int fileSize = fileInputStream.available();
        byte[] buffer = new byte[fileSize];

        while (fileInputStream.available() > 0) {
            fileInputStream.read(buffer);
        }
        String s="";
        for(byte bb: buffer){
            s += ((char)bb);
        }

        String[] words = s.split("[\\p{Punct}\\s]+"); // split by words except punctuations

        int count = 0;
        for (String word: words){
            if((word.toLowerCase()).equals("world")){
                count++;
            }
        }
        bufferedReader.close();
        fileInputStream.close();
        System.out.println(count);
    }
}

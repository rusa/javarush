package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();

        FileReader fileReader = new FileReader(fileName1);
        FileWriter fileWriter = new FileWriter(fileName2);

        String s = "";
        while (fileReader.ready()){
            int data = fileReader.read();
            s+=(char)data;
        }
        String[] words = s.split(" ");

        for(String w: words){
            if(w.matches("\\d+")){  // check is numeric
                fileWriter.write(w+" ");
            }
        }
        bufferedReader.close();
        fileReader.close();
        fileWriter.close();
    }
}

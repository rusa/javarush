package com.javarush.test.level19.lesson08.task02;

/* Ридер обертка 2
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна заменять все подстроки "te" на "??"
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток
Вывести модифицированную строку в консоль.
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream oldSystemOut = System.out;
        PrintStream newSystemOut = new ReplaceTEPrintStream(System.out);
        System.setOut(newSystemOut);

        testString.printSomething();

        System.setOut(oldSystemOut);
    }

    public static class ReplaceTEPrintStream extends PrintStream {
        private PrintStream stream;

        public ReplaceTEPrintStream (PrintStream stream) {
            super(stream);
            this.stream = stream; // отличие здесь
        }

        @Override
        public void println(String x) {
            stream.println(x.replaceAll("te", "??"));
        }

        @Override
        public void close() {
            stream.close();
        }
    }


//    public static void main(String[] args) {
//        PrintStream consoleStream = System.out;
//
//        //Создаем динамический массив
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//
//        //создаем адаптер к классу PrintStream
//        PrintStream stream = new PrintStream(outputStream);
//        //Устанавливаем его как текущий System.out
//        System.setOut(stream);
//
//        testString.printSomething();
//
//        //Преобразовываем записанные в наш ByteArray данные в строку
//        String result = outputStream.toString();
//
//        //Возвращаем все как было
//        System.setOut(consoleStream);
//
//        //переводим в верхний регистр
//        StringBuilder stringBuilder = new StringBuilder(result);
//        //stringBuilder.toUpperCase();
//        String reverseString = stringBuilder.toString().replaceAll("te", "??");
//
//        //выводим ее в консоль
//        System.out.println(reverseString);
//
//    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
    }
    }
}
//        У вас есть водопроводный кран (поток) вы его открыли и подставили ведро (System.setOut(stream); перенаправили в ByteArrayOutputStream) потом кран закрыли и ведро убрали (System.setOut(consoleStream)). Добавили чего-то в ведро и вылили куда вам надо
//        String result = outputStream.toString().toUpperCase();
//        System.out.println(result);
package com.javarush.test.level19.lesson08.task04;

/* Решаем пример
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить на консоль решенный пример
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Возможные операции: + - *
Шаблон входных данных и вывода: a [знак] b = c
Отрицательных и дробных чисел, унарных операторов - нет.

Пример вывода:
3 + 6 = 9
*/

import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream oldSystemOut = System.out;
        PrintStream newSystemOut = new DoSumStream(System.out);
        System.setOut(newSystemOut);

        testString.printSomething();

        System.setOut(oldSystemOut);

    }

    public static class DoSumStream extends PrintStream {

        private PrintStream stream;

        DoSumStream (PrintStream origStream){
            super(origStream);
            this.stream = origStream;
        }

        @Override
        public void println(String x) {
            String[] operands = x.split(" ");

            Integer a = Integer.parseInt(operands[0]);
            Integer b = Integer.parseInt(operands[2]);
            String operator = operands[1];
            switch (operator) {
                case "+":
                    stream.println(x + (a + b));
                    break;
                case "-":
                    stream.println(x + (a - b));
                    break;
                case "*":
                    stream.println(x + (a * b));
                    break;
            }
        }

        @Override
        public void close() {
            stream.close();
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}
//        У вас есть водопроводный кран (поток) вы его открыли и подставили ведро (System.setOut(stream); перенаправили в ByteArrayOutputStream) потом кран закрыли и ведро убрали (System.setOut(consoleStream)). Добавили чего-то в ведро и вылили куда вам надо
//        String result = outputStream.toString().toUpperCase();
//        System.out.println(result);
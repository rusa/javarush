package com.javarush.test.level19.lesson03.task04;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/* И еще один адаптер
Адаптировать Scanner к PersonScanner.
Классом-адаптером является PersonScannerAdapter.
Данные в файле хранятся в следующем виде:
Иванов Иван Иванович 31 12 1950

В файле хранится большое количество людей, данные одного человека находятся в одной строке. Метод read() должен читать данные одного человека.
*/

public class Solution {
    public static class PersonScannerAdapter implements PersonScanner {
        private Scanner scanner;
        public PersonScannerAdapter(Scanner scanner) {
            this.scanner = scanner;
        }

        @Override
        public Person read() throws IOException {
            String person = scanner.nextLine();

            String[] personData = person.split(" ");
            int day = Integer.parseInt(personData[3]);
            int month = Integer.parseInt(personData[4])-1;
            int year = Integer.parseInt(personData[5]);
            Calendar date = new GregorianCalendar(year,month,day);
            return new Person(personData[1], personData[2], personData[0], date.getTime());
        }

        @Override
        public void close() throws IOException {
            this.scanner.close();
        }
    }

    public static void main(String[] args) {
//        String[] personData = "Иванов Иван Иванович 31 12 1950".split(" ");
//        for(String s: personData){
//            System.out.println(s);
//        }
        System.out.println("+38(050)123-45-67".replaceAll("[\\s\\-()]", ""));
        String[] personData1 = "Ivanov, Ivan".split(", ");
        for(String s: personData1){
            System.out.println(s);
        }
    }
}
